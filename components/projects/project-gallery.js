import React, { useState } from 'react'

import Image from 'next/image'

import Lightbox from 'lightbox-react'

const ProjectGallery = ({ images }) => {
  const [touchStart, setTouchStart] = useState(0)
  const [touchEnd, setTouchEnd] = useState(0)

  const allImages = images

  const handleTouchStart = (e) => {
    setTouchStart(e.targetTouches[0].clientX)
  }

  const handleTouchMove = (e) => {
    setTouchEnd(e.targetTouches[0].clientX)
  }

  const handleTouchEnd = () => {
    if (touchStart - touchEnd > 100) {
      // do your stuff here for left swipe
      setLightboxIndex((lightboxIndex + 1) % lightboxImages.length)
    }

    if (touchStart - touchEnd < -100) {
      // do your stuff here for right swipe
      setLightboxIndex(
        (lightboxIndex + lightboxImages.length - 1) % lightboxImages.length
      )
    }
  }

  const imgArray = images
  const amountOfImages = imgArray.length

  const mappedImages = imgArray.map((node, i) => {
    let imageClassNames =
      'inline-flex rounded-lg h-auto overflow-hidden gradient-mask-t-0% shadow-lg cursor-pointer hover:shadow-xl dark:text-gray-50'

    if (amountOfImages === 1) {
      imageClassNames = `${imageClassNames} col-start-1 mx-auto sm:col-start-1 md:col-start-1 md:mx-24 lg:col-start-2 lg:-mx-20`
    }

    if (amountOfImages === 3) {
      imageClassNames = `${imageClassNames} col-start-0 sm:col-start-0 md:col-start-0 lg:col-start-0`
    }

    if (amountOfImages === 2 && i === 0) {
      imageClassNames = `${imageClassNames} col-start-0 sm:col-start-2 md:col-start-2 lg:col-start-3`
    }

    return (
      <div
        className={`${imageClassNames}`}
        onClick={() => {
          setLightboxIndex(i)
          setIsOpen(true)
        }}
        aria-hidden='true'
        key={`gallery-image-${i}`}
      >
        <Image
          className={'rounded-lg'}
          src={node.url}
          objectFit={'cover'}
          width={node.width}
          height={node.height}
          placeholder={'blur'}
          blurDataURL={`${node.url}?w=8`}
        />
      </div>
    )
  })

  const lightboxImages = allImages.map((node, i) => {
    return (
      <div
        className='justify-center w-screen h-screen max-w-full md:pb-8'
        key={`lightbox-image-${i}`}
      >
        <Image
          onTouchEnd={handleTouchEnd}
          onTouchStart={handleTouchStart}
          onTouchMove={handleTouchMove}
          src={node.url}
          objectFit={'contain'}
          // width={node.width}
          // height={node.height}
          layout={'fill'}
          key={`lightbox-image-grid-photo-${i}`}
          className='w-full h-full'
          alt={node.description || 'No image alt text available.'}
          placeholder={'blur'}
          blurDataURL={`${node.url}?w=8`}
        />
      </div>
    )
  })

  const [lightboxIndex, setLightboxIndex] = useState(-1)
  const [isOpen, setIsOpen] = useState(false)
  let containerClassNames = 'grid gap-4 mx-auto mt-4 mb-8'
  if (amountOfImages === 1) {
    containerClassNames = `${containerClassNames} grid-cols-1 sm:grid-cols-1 md:grid-cold-3 lg:grid-cols-3`
  }
  if (amountOfImages === 2) {
    containerClassNames = `${containerClassNames} grid-cols-2 sm:grid-cols-4 md:grid-cold-5 lg:grid-cols-4`
  }
  if (amountOfImages === 3) {
    containerClassNames = `${containerClassNames} grid-cols-3 sm:grid-cols-3 md:grid-cold-3 lg:grid-cols-3`
  }
  if (amountOfImages > 3) {
    containerClassNames = `${containerClassNames} grid-cols-4 sm:grid-cols-5 md:grid-cols-5 lg:grid-cols-5`
  }

  return (
    <>
      <div className={`${containerClassNames}`}>{mappedImages}</div>
      {isOpen && (
        <Lightbox
          mainSrc={lightboxImages[lightboxIndex]}
          nextSrc={lightboxImages[(lightboxIndex + 1) % lightboxImages.length]}
          prevSrc={
            lightboxImages[
              (lightboxIndex + lightboxImages.length - 1) %
                lightboxImages.length
            ]
          }
          // animationDuration={500}
          onCloseRequest={() => setIsOpen(false)}
          onMovePrevRequest={() =>
            setLightboxIndex(
              (lightboxIndex + lightboxImages.length - 1) %
                lightboxImages.length
            )
          }
          onMoveNextRequest={() =>
            setLightboxIndex((lightboxIndex + 1) % lightboxImages.length)
          }
          enableZoom={false}
        />
      )}
    </>
  )
}

export default ProjectGallery
