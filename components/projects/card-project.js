import { useState } from 'react'

import Image from 'next/image'
import Link from 'next/link'

import Icons from '@components/ui/icons'
import Richtext from '@components/ui/richtext'

function CardProject(props) {
  const { data } = props

  const [isOpen, setIsOpen] = useState(false)

  return (
    <Link href={`/ausbau/projekte/${data.slug}`}>
      <div className='flex items-center justify-start w-full p-0 my-5 sm:w-1/2 lg:w-full sm:inline-flex'>
        <div className='max-w-full p-0 tracking-normal bg-white rounded-lg shadow-lg cursor-pointer hover:shadow-2xl'>
          <div className='relative flex flex-col lg:flex-row'>
            <div
              className={
                'rounded-t-lg lg:rounded-l-lg lg:rounded-t-none overflow-hidden flex relative lg:max-w-sm'
              }
            >
              <Image
                src={`${data.heroImage.url}`}
                // layout={'fill'}
                height={data.heroImage.height}
                width={data.heroImage.width}
                placeholder={'blur'}
                blurDataURL={`${data.heroImage.url}?w=10`}
                className={'absolute left-0 top-0 p-0 m-0 w-full h-full'}
                objectFit={'cover'}
                objectPosition={'center'}
              />
            </div>
            <div
              className={
                'relative p-2 inline-flex flex-col h-56 sm:h-20 md:h-56 overflow-hidden'
              }
            >
              <div id='card-title' className={'px-3 pt-2 pb-0'}>
                <h3 id='name' className='mb-0 text-xl font-semibold'>
                  {data.title}
                </h3>
              </div>
              <div id='card-body' className={`px-3`}>
                <p
                  id='text'
                  className='inline-flex max-w-xl mt-2 tracking-tight text-gray-800'
                >
                  <Richtext content={data.text} />
                </p>
              </div>
              <div
                className={
                  'absolute flex justify-center items-end bg-gradient-to-b from-transparent font-semibold via-transparent to-white w-full h-full top-0 left-0 cursor-pointer rounded-lg'
                }
              />
            </div>
            <div
              className={
                'absolute bottom-0 right-0 rounded-br-lg bg-yellow-400 px-3 py-0 shadow'
              }
            >
              <Link href={`/ausbau/projekte/${data.slug}`}>
                <a className={'h-8 flex justify-center items-center'}>
                  Projekt anzeigen
                </a>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </Link>
  )
}

export default CardProject
