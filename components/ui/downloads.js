import Icon from '@components/ui/icons'
import StyledLink from '@components/ui/link'

function Downloads(props) {
  const files = props.content

  return (
    <div>
      <h1>Dateien herunterladen</h1>
      {files.map((file, i) => (
        <div
          className={
            'flex-wrap md:flex-nowrap flex border-b-2 border-grey-900 md:border-0 md:my-2'
          }
        >
          <span className={'bg-gray-100 pt-4 md:py-4 px-2 md:px-6 w-full'}>
            <span className={'font-bold'}>{file.title}</span>
            <br />
            <span>{file.description}</span>
          </span>
          <span className={'bg-gray-100 pt-2 pb-4 md:py-4 px-2 md:px-6 w-full md:w-1/2 flex flex-auto justify-start md:justify-end'}>
            <StyledLink
              icon={file.contentType}
              target={'_blank'}
              href={file.url}
              text={'Herunterladen'}
            />
          </span>
        </div>
      ))}
    </div>
  )
}

export default Downloads
