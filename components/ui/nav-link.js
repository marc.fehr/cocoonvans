import { useState } from 'react'
import Link from 'next/link'

import { withUserAgent } from 'next-useragent'

import Icons from './icons'

function NavLink(props) {
  // Get browser user agent data
  const { ua } = props

  const { data } = props
  const [isOpen, setIsOpen] = useState(false)

  function handleClick(show) {
    if (!show) {
      setIsOpen(false)
    } else {
      setIsOpen(true)
    }
  }

  const getLinkTarget = (link) => {
    if (link.includes('http')) {
      return '_blank'
    } else {
      return '_self'
    }
  }

  if (data.children.length <= 0) {
    return (
      <Link href={data.link}>
        <a
          target={getLinkTarget(data.link)}
          className='items-center justify-center w-full px-3 py-2 font-bold text-white lg:inline-flex lg:w-auto lg:hover:bg-yellow-400 hover:text-gray-800 group'
          onClick={() => props.menuHandler(false)}
        >
          {data.icon && (
            <span
              className={
                'sm:hidden inline-flex w-5 mr-3 lg:mr-1 text-white group-hover:text-gray-800'
              }
            >
              <Icons icon={data.icon} />
            </span>
          )}
          {data.title}
        </a>
      </Link>
    )
  } else {
    return (
      <Link
        href={data.link}
        className={'group'}
        target={getLinkTarget(data.link)}
      >
        <a
          target={getLinkTarget(data.link)}
          className={`relative items-center justify-center w-full px-3 py-2 font-bold text-white lg:inline-flex lg:w-auto lg:hover:bg-yellow-400 ${
            isOpen ? '' : 'group'
          }`}
          onClick={() => {
            handleClick(!isOpen)
          }}
          onMouseEnter={() => {
            if (ua.isDesktop) {
              handleClick(true)
            } else return null
          }}
          onMouseLeave={() => {
            if (ua.isDesktop) {
              handleClick(false)
            } else return null
          }}
        >
          {data.icon && (
            <span className={'inline-flex w-5 mr-3 lg:mr-1 text-white'}>
              <Icons icon={data.icon} />
            </span>
          )}
          <span className={'group-hover:text-white'}>{data.title}</span>
          {isOpen ? (
            <>
              <Icons icon={'chevron-down'} />
              <div
                onClick={() => props.menuHandler(false)}
                className={
                  'relative inline-block mt-3 lg:mt-0 w-full lg:absolute lg:top-full bg-gray-700 shadow-xl z-50'
                }
              >
                {data.children.map((subLink) => (
                  <Link href={subLink.link} key={`sub-link-${subLink.title}`}>
                    <a className='items-center justify-center block w-full px-3 py-2 font-bold text-white lg:inline-flex lg:w-full lg:hover:bg-yellow-400 hover:text-gray-800 group'>
                      {subLink.icon && (
                        <span
                          className={
                            'inline-flex w-5 mr-3 lg:mr-1 text-white group-hover:text-gray-800'
                          }
                        >
                          <Icons icon={subLink.icon} />
                        </span>
                      )}
                      {subLink.title}
                    </a>
                  </Link>
                ))}
              </div>
            </>
          ) : (
            <Icons icon={'chevron-right'} />
          )}
        </a>
      </Link>
    )
  }
}

export default withUserAgent(NavLink)
