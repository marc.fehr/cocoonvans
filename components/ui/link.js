import Link from 'next/link'

import Icon from '@components/ui/icons'

function StyledLink(props) {
  return (
    <Link href={props.href}>
      <a
        target={
          props.target
            ? props.target
            : props.href.includes('http')
            ? '_blank'
            : '_self'
        }
        className={'font-bold my-auto'}
      >
        {props.icon && (
          <span className={'inline-block mr-2 w-8'}>
            {props.icon && <Icon icon={props.icon} />}
          </span>
        )}
        <span className={'border-b-2 border-yellow-400 hover:bg-yellow-400'}>
          {props.text}
        </span>
      </a>
    </Link>
  )
}

export default StyledLink
