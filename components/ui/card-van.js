import { useContext, useEffect, useState } from 'react'

import Moment from 'moment'
import { extendMoment } from 'moment-range'

import RentalContext from '@store/rental-context'
import NotificationContext from '@store/notification-context'

import StyledLink from '@components/ui/link'

const VanCard = (props) => {
  const rentalCtx = useContext(RentalContext)
  const notificationCtx = useContext(NotificationContext)

  const moment = extendMoment(Moment)

  const [isAvailable, setIsAvailable] = useState(true)
  const [isFetching, setIsFetching] = useState(false)

  useEffect(() => {
    // 0 Set data for booking from ctx
    const dateFrom = rentalCtx.dateFrom?.format('YYYY-MM-DD')
    const dateTo = rentalCtx.dateTo?.format('YYYY-MM-DD')
    const rangeBooking = moment.range(dateFrom, dateTo)

    // 1 Set the fetching state
    setIsFetching(true)
    setIsAvailable(true)

    notificationCtx.showNotification({
      title: 'Lade Daten...',
      message: 'Bitte warten',
      status: 'pending',
    })

    // 2 Check all vans for availability with their cals
    fetch(`/api/rental/${props.data.calendarId}/${dateFrom}/${dateTo}`)
      .then((res) => res.json())
      .then((data) => {
        for (let i = 0; i < data.events.length; i++) {
          const bookedFrom = data.events[i].start.date
          const bookedTo = data.events[i].end.date
          const rangeBooked = moment.range(bookedFrom, bookedTo)

          const overlap = rangeBooking.overlaps(rangeBooked)

          // 3 Set availability based on moment range check
          if (overlap) {
            console.log(rangeBooked)
            setIsAvailable(false)
          }
        }
        setIsFetching(false)
        notificationCtx.hideNotification()
      })
  }, [rentalCtx, props.data])

  return (
    <div className={'border-b-2 pb-5 mb-2'}>
      <h1>{props.data.title}</h1>
      {isAvailable ? (
        <StyledLink href={`/vans/${props.data.slug}`} text={'Jetzt buchen'} />
      ) : (
        <p>Nicht verfügbar</p>
      )}
      {isFetching && <p>Kalender wird geprüft...</p>}
    </div>
  )
}

export default VanCard
