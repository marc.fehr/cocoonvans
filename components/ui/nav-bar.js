import { useState } from 'react'
import Link from 'next/link'

import Logo from './logo'
import NavLink from './nav-link.js'
import NavLinks from '../../data/links'

export const Navbar = () => {
  const [isOpen, setIsOpen] = useState(false)

  const handleClick = (isOpen) => {
    setIsOpen(isOpen)
  }

  return (
    <>
      <nav className='flex flex-wrap items-center justify-center p-2 bg-gray-800'>
        <Link href='/'>
          <a className='inline-flex items-center p-1'>
            <div className={'w-60'}>
              <Logo />
            </div>
          </a>
        </Link>
        <button
          className='inline-flex p-3 ml-auto text-yellow-300 outline-none text- hover:bg-yellow-400 lg:hidden hover:text-gray-800'
          onClick={() => handleClick(!isOpen)}
        >
          <svg
            className='w-6 h-6'
            fill='none'
            stroke='currentColor'
            viewBox='0 0 24 24'
            xmlns='http://www.w3.org/2000/svg'
          >
            <path
              strokeLinecap='round'
              strokeLinejoin='round'
              strokeWidth={2}
              d='M4 6h16M4 12h16M4 18h16'
            />
          </svg>
        </button>
        <div
          className={`${
            isOpen ? '' : 'hidden'
          } mt-4 lg:mt-0 w-full lg:inline-flex lg:flex-grow lg:w-auto`}
        >
          <div className='flex flex-col items-start w-full lg:inline-flex lg:flex-row lg:ml-auto lg:w-auto lg:items-center lg:h-auto'>
            {NavLinks.map((navLink, i) => (
              <NavLink
                key={`nav-link-${i}`}
                data={navLink}
                menuHandler={handleClick}
              />
            ))}
          </div>
        </div>
      </nav>
    </>
  )
}

export default Navbar
