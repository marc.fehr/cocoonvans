import { Fragment, useContext } from 'react'

import NotificationContext from '@store/notification-context'
import Notification from '@components/ui/notification'
import NavBar from './nav-bar'
import Footer from './footer'

function Layout(props) {
  const { children } = props

  const notificationCtx = useContext(NotificationContext)
  const activeNotification = notificationCtx.notification

  return (
    <Fragment>
      <section className={'min-h-screen'}>
        <NavBar />
        <main className={'md:my-12'}>{children}</main>
      </section>
      {activeNotification && (
        <Notification
          title={activeNotification.title}
          message={activeNotification.message}
          status={activeNotification.status}
        />
      )}
      <Footer />
    </Fragment>
  )
}

export default Layout
