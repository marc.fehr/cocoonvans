import styles from './loading.module.css'

function LoadingSpinner(props) {
  return (
    <div
      className={
        'bg-transparent p-2 m-auto text-dark-800 dark:text-white items-center flex flex-col justify-center'
      }
    >
      <div className={`${styles.loading} inline relative h-10 w-10`}>
        <div />
        <div />
        <div />
        <div />
      </div>
      {props.message && <p className={'text-gray-500 m-0  p-0 dark:text-gray-50'}>{props.message}</p>}
    </div>
  )
}

export default LoadingSpinner
