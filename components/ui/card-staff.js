import { useState } from 'react'

import Image from 'next/image'

import Icons from '@components/ui/icons'
import Richtext from '@components/ui/richtext'

function CardStaff(props) {
  const { data } = props

  const [isOpen, setIsOpen] = useState(false)

  return (
    <div className='flex items-center justify-start w-full p-2 mb-5 sm:w-1/2 lg:w-full sm:inline-flex'>
      <div className='max-w-full p-0 tracking-normal bg-white rounded-lg shadow-lg'>
        <div className='flex flex-col lg:flex-row'>
          <div
            className={
              'rounded-t-lg lg:rounded-l-lg lg:rounded-t-none overflow-hidden flex relative lg:max-w-sm'
            }
          >
            <Image
              src={`${data.image.url}`}
              // layout={'fill'}
              height={data.image.height}
              width={data.image.width}
              placeholder={'blur'}
              blurDataURL={`${data.image.url}?w=5`}
              className={'absolute left-0 top-0 p-0 m-0 w-full h-full'}
              objectFit={'cover'}
              objectPosition={'center'}
            />
          </div>
          <div className={'relative p-2 inline-flex flex-col'}>
            <div id='card-title' className={'px-3 pt-2 pb-0'}>
              <h4 id='name' className='mb-0 text-xl font-semibold'>
                {data.firstname} {data.name}, {data.title}
              </h4>
            </div>
            <div
              id='card-body'
              className={`px-3 ${
                isOpen ? 'max-h-full' : 'max-h-40 overflow-hidden lg:max-h-full'
              }`}
            >
              <p
                id='text'
                className='inline-flex max-w-2xl mt-2 tracking-tight text-gray-800'
              >
                <Richtext content={data.text} />
              </p>
              {!isOpen ? (
                <div
                  onClick={() => setIsOpen(true)}
                  className={
                    'absolute flex justify-center items-end bg-gradient-to-b from-transparent font-semibold to-white w-full h-full top-0 left-0 cursor-pointer lg:hidden'
                  }
                >
                  <p>Mehr anzeigen <Icons icon={'chevron-down'} /></p>
                </div>
              ) : (
                <div
                  className={
                    'flex justify-center items-end font-semibold cursor-pointer'
                  }
                  onClick={() => setIsOpen(false)}
                >
                  <p>Weniger anzeigen <Icons icon={'chevron-up'} /></p>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default CardStaff
