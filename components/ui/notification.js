import { useContext } from 'react'

import NotificationContext from '@store/notification-context'

function Notification(props) {
  const notificationCtx = useContext(NotificationContext)

  const { title, message, status } = props

  let statusClasses = ''

  if (status === 'success') {
    statusClasses = 'bg-green-300 text-gray-800'
  }

  if (status === 'error') {
    statusClasses = 'bg-red-400'
  }

  if (status === 'pending') {
    statusClasses = 'bg-yellow-400 text-gray-800'
  }

  return (
    <div
      className={`shadow-lg px-5 py-5 md:py-0 text-white cursor-pointer md:justify-between justify-around fixed bottom-0 left-0 h-20 w-full flex flex-col md:flex-row items-center ${statusClasses}`}
      onClick={notificationCtx.hideNotification}
    >
      <h2>{title}</h2>
      <p className={'m-0 p-0'}>{message}</p>
    </div>
  )
}

export default Notification
