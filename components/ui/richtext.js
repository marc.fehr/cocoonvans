import React from 'react'
import { BLOCKS, MARKS, INLINES } from '@contentful/rich-text-types'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'
import Image from 'next/image'
import Link from 'next/link'

const Richtext = (props) => {
  const { content } = props

  const assets = content.links?.assets?.block
  const getAssetById = (assetId) => {
    return assets.filter((asset) => asset.sys.id === assetId)[0]
  }

  const options = {
    renderMark: {
      [MARKS.BOLD]: (text) => <strong>{text}</strong>,
      [BLOCKS.HEADING_1]: (text) => <h1>{text}</h1>,
      [BLOCKS.HEADING_2]: (text) => <h2>{text}</h2>,
      [BLOCKS.HEADING_3]: (text) => <h3>{text}</h3>,
      [BLOCKS.HEADING_4]: (text) => <h4>{text}</h4>,
      [BLOCKS.HEADING_5]: (text) => <h5>{text}</h5>,
      [BLOCKS.HEADING_6]: (text) => <h6>{text}</h6>,
      [BLOCKS.LIST_ITEM]: (text) => <li>{text}</li>,
      [BLOCKS.UL_LIST]: (text) => <ul>{text}</ul>,
      [BLOCKS.OL_LIST]: (text) => <ol>{text}</ol>,
    },
    renderNode: {
      [INLINES.EMBEDDED_ENTRY]: (content, e) => {
        return <div key={Math.random()}>Content!</div>
      },
      [INLINES.HYPERLINK]: (link) => {
        return (
          <Link href={link?.data?.uri} key={Math.random()}>
            <a
              className={
                'font-bold border-b-2 border-yellow-400 hover:bg-yellow-400'
              }
              target={link?.data?.uri?.includes('http') ? '_blank' : '_self'}
            >
              {link.content[0].value}
            </a>
          </Link>
        )
      },
      [BLOCKS.EMBEDDED_ASSET]: (node) => {
        const assetId = node.data.target.sys.id
        const { width, height, fileName, url, title, description } =
          getAssetById(assetId)

        return (
          <figure key={assetId}>
            <Image
              key={fileName}
              src={url}
              height={height}
              width={width}
              alt={title}
              className={'rounded-sm inline'}
            />
            {description && <figcaption>{description}</figcaption>}
          </figure>
        )
      },
    },
    renderText: (text) =>
      text.split('\n').flatMap((text, i) => [i > 0 && <br />, text]),
  }

  return (
    <div className={props.className}>
      {documentToReactComponents(content.json, options)}
    </div>
  )
}

export default Richtext
