function TextContainer(props) {
  const { children } = props

  return <section className={'px-3 py-2 md:p-0 max-w-screen-md mx-auto'}>{children}</section>
}

export default TextContainer
