import { useState, useContext } from 'react'

import { withUserAgent } from 'next-useragent'
import RentalContext from '@store/rental-context'

import 'react-dates/initialize'
import { DateRangePicker } from 'react-dates'
import 'react-dates/lib/css/_datepicker.css'

import Moment from 'moment'
import { extendMoment } from 'moment-range'
import deLocale from 'moment/locale/de'

import LoadingSpinner from '@components/ui/loading'

// Set German locale for Moment JS
const moment = extendMoment(Moment)
moment.updateLocale('de', deLocale)

function DatePicker(props) {
  // Get browser user agent data
  const { calendarEvents, ua } = props

  const [focus, setFocus] = useState(null)
  const rentalCtx = useContext(RentalContext)

  const dateHandler = (dates) => {
    const { startDate, endDate } = dates
    if (startDate) {
      rentalCtx.dateFromHandler(startDate)
    }
    if (endDate) {
      rentalCtx.dateToHandler(endDate)
    }
  }

  const isDayBlocked = (day) => {
    // Loop through blocked days from Google Cal and check if in array
    return props.calendarEvents.some((unavailableDay) =>
      moment(unavailableDay).isSame(day, 'day')
    )
  }

  return (
    <div>
      <div className={`p-5 bg-yellow-200 dark:bg-gray-600`}>
        {calendarEvents ? (
          <DateRangePicker
            openDirection={'up'}
            startDatePlaceholderText='Abholung'
            startDate={rentalCtx.dateFrom || null}
            endDatePlaceholderText='Rückgabe'
            onDatesChange={dateHandler}
            endDate={rentalCtx.dateTo || null}
            numberOfMonths={ua?.isDesktop ? 2 : 1}
            displayFormat='DD.MM.YYYY'
            focusedInput={focus}
            onFocusChange={(focus) => setFocus(focus)}
            startDateId='startDateRental'
            endDateId='endDateRental'
            minimumNights={3}
            hideKeyboardShortcutsPanel
            readOnly
            isDayBlocked={(day) => isDayBlocked(day)}
            // keepOpenOnDateSelect
            // showClearDates
            // reopenPickerOnClearDates
          />
        ) : (
          <LoadingSpinner message={'Lade Kalender...'} />
        )}
      </div>
      <div
        className={
          'flex flex-col px-5 py-3 bg-gray-100 dark:bg-gray-700 dark:text-gray-50'
        }
      >
        <div className={'flex justify-between items-center'}>
          <span>Abholung:</span>
          <span className={'bg-gray-200 py-1 px-2 mb-1 dark:bg-gray-600 rounded'}>{rentalCtx.dateFrom?.format('Do MMMM YYYY')}</span>
        </div>
        <div className={'flex justify-between'}>
          <span>Rückgabe:</span>
          <span className={'bg-gray-200 py-1 px-2 mt-1 dark:bg-gray-600 rounded'}>{rentalCtx.dateTo?.format('Do MMMM YYYY')}</span>
        </div>
      </div>
    </div>
  )
}

export default withUserAgent(DatePicker)
