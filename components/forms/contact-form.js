import { useState, useRef, useContext } from 'react'

import NotificationContext from '@store/notification-context'

function ContactForm() {
  const [error, setError] = useState({})
  const [isSent, setIsSent] = useState(false)

  const firstnameInputRef = useRef()
  const lastnameInputRef = useRef()
  const phoneInputRef = useRef()
  const placeInputRef = useRef()
  const emailInputRef = useRef()
  const messageInputRef = useRef()

  const notificationCtx = useContext(NotificationContext)

  async function postData(url = '', data = {}) {
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/json',
      },
      referrerPolicy: 'no-referrer',
      body: JSON.stringify(data),
    })
    return response
  }

  const processMessage = (event) => {
    event.preventDefault()

    // Validation to be done here TODO
    // If data valid, send POST request

    notificationCtx.showNotification({
      title: 'Senden...',
      message: 'Bitte warten',
      status: 'pending',
    })

    postData('/api/contact/send-email', {
      message: messageInputRef.current.value,
      email: emailInputRef.current.value,
      phone: phoneInputRef.current.value,
      city: placeInputRef.current.value,
      name: `${firstnameInputRef.current.value} ${lastnameInputRef.current.value}`,
    })
      .then(async (res) => {
        if (res.ok) {
          return res.json()
        }
        const data = await res.json()
        throw new Error(data.message || 'Da ging etwas schief...')
      })
      .then((data) => {
        notificationCtx.showNotification({
          title: '🤘🏼 Vielen Dank',
          message: 'Deine Nachricht ist bei uns angekommen!',
          status: 'success',
        })
        setIsSent(true)
      })
      .catch((error) => {
        notificationCtx.showNotification({
          title: 'Oops!',
          message: error.message || 'Da ging etwas schief...',
          status: 'error',
        })
      })
  }

  return (
    <form
      className='w-full'
      onSubmit={processMessage}
    >
      <input disabled={isSent} type='hidden' name='form-name' value='contact' />
      <p className='hidden'>
        <label>
          Don’t fill this out if you’re human:{' '}
          <input disabled={isSent} name='bot-field' />
        </label>
      </p>
      <div className='flex flex-wrap mt-8 -mx-3'>
        <div className='w-full px-3 mb-6 md:w-1/2 md:mb-0'>
          <label
            className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase'
            htmlFor='grid-first-name'
          >
            Vorname*
          </label>
          <input
            disabled={isSent}
            ref={firstnameInputRef}
            className='block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border /*border-yellow-500*/ rounded appearance-none focus:outline-none focus:bg-white'
            id='grid-first-name'
            type='text'
            name='vorname'
            placeholder='Josef'
            autoComplete='firstname'
          />
          {error.firstname && (
            <p className='text-xs italic text-yellow-500'>
              Please fill out this field.
            </p>
          )}
        </div>

        <div className='w-full px-3 md:w-1/2'>
          <label
            className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase'
            htmlFor='grid-last-name'
          >
            Nachname*
          </label>
          <input
            disabled={isSent}
            ref={lastnameInputRef}
            className='block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500'
            id='grid-last-name'
            type='text'
            name='nachname'
            placeholder='Meister'
            autoComplete='lastname'
          />
          {error.lastname && (
            <p className='text-xs italic text-yellow-500'>
              Please fill out this field.
            </p>
          )}
        </div>
      </div>

      <div className='flex flex-wrap -mx-3'>
        <div className='w-full px-3 mb-6 md:w-1/2 md:mb-0'>
          <label
            className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase'
            htmlFor='grid-phone'
          >
            Telefon / WhatsApp*
          </label>
          <input
            disabled={isSent}
            ref={phoneInputRef}
            autoComplete='phone'
            className='block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border /*border-yellow-500*/ rounded appearance-none focus:outline-none focus:bg-white'
            id='grid-phone'
            type='phone'
            name='telefon'
            placeholder='079 123 66 66'
          />
          {error.firstname && (
            <p className='text-xs italic text-yellow-500'>
              Please fill out this field.
            </p>
          )}
        </div>

        <div className='w-full px-3 md:w-1/2'>
          <label
            className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase'
            htmlFor='grid-place'
          >
            Wohnort
          </label>
          <input
            disabled={isSent}
            ref={placeInputRef}
            autoComplete='town'
            className='block w-full px-4 py-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500'
            id='grid-place'
            type='text'
            name='wohnort'
            placeholder='Wunderland'
          />
          {error.lastname && (
            <p className='text-xs italic text-yellow-500'>
              Please fill out this field.
            </p>
          )}
        </div>
      </div>

      <div className='flex flex-wrap mb-6 -mx-3'>
        <div className='w-full px-3'>
          <label
            className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase'
            htmlFor='grid-email'
          >
            E-mail*
          </label>
          <input
            disabled={isSent}
            autoComplete='email'
            ref={emailInputRef}
            className='block w-full px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none focus:outline-none focus:bg-white focus:border-gray-500'
            id='grid-email'
            type='email'
            name='email'
            placeholder={'dein-name@gmail.com'}
          />
          <p className='text-xs italic text-gray-600'>
            {error.email && (
              <p className='text-xs italic text-yellow-500'>
                Bitte gültige E-Mail-Adresse angeben
              </p>
            )}
          </p>
        </div>
      </div>

      <div className='flex flex-wrap mb-6 -mx-3'>
        <div className='w-full px-3'>
          <label
            className='block mb-2 text-xs font-bold tracking-wide text-gray-700 uppercase'
            htmlFor='grid-message'
          >
            Deine Nachricht*
          </label>
          <textarea
            disabled={isSent}
            ref={messageInputRef}
            placeholder={'Lass uns wissen, was wir für dich tun können :)'}
            className='block w-full h-48 px-4 py-3 mb-3 leading-tight text-gray-700 bg-gray-200 border border-gray-200 rounded appearance-none resize-none no-resize focus:outline-none focus:bg-white focus:border-gray-500'
            id='grid-message'
            name='message'
          />
          {error.message === '' && (
            <p className='text-xs italic text-yellow-500'>
              Bitte eine Nachricht eingeben
            </p>
          )}
        </div>
      </div>

      {!isSent && (
        <div className='md:flex md:items-center'>
          <div className='md:w-1/3'>
            <button
              className='px-4 py-2 font-bold bg-yellow-400 rounded shadow text-grey-darkest hover:bg-yellow-300 focus:shadow-outline focus:outline-none dark:text-gray-800'
              type='submit'
            >
              📬 &nbsp;Ab die Post
            </button>
          </div>
        </div>
      )}
    </form>
  )
}

export default ContactForm
