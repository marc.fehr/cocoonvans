const colors = require('tailwindcss/colors')
const iOSHeight = require('@rvxlab/tailwind-plugin-ios-full-height')

module.exports = {
  purge: ['./pages/**/*.{js,ts,jsx,tsx}', './components/**/*.{js,ts,jsx,tsx}'],
  darkMode: 'media',
  theme: {
    aspectRatio: {
      // defaults to {}
      none: 0,
      square: [1, 1], // or 1 / 1, or simply 1
      '16/9': [16, 9], // or 16 / 9
      '4/3': [4, 3], // or 4 / 3
      '21/9': [21, 9], // or 21 / 9
    },
    extend: {
      zIndex: {
        '-1': '-1',
      },
      maxHeight: {
        0: '0',
        '1/4': '25vh',
        '1/2': '50vh',
        '3/4': '75vh',
        full: '100vh',
      },
      colors: {
        brand: {
          light: '#83a6cc',
          DEFAULT: '#4371a3',
          dark: '#345a84',
        },
        blue: colors.lightBlue,
        yellow: colors.amber,
        green: colors.teal,
        rose: colors.rose,
      },
      animation: {
        wiggle: 'wiggle 1s ease-in-out infinite',
        rotate: 'rotate 30s linear infinite',
      },
      keyframes: {
        wiggle: {
          '0%, 100%': { transform: 'rotate(-3deg)' },
          '50%': { transform: 'rotate(3deg)' },
        },
        rotate: {
          '0%': { transform: 'rotate(0deg)' },
          '100%': { transform: 'rotate(360deg)' },
        },
      },
      maxHeight: {
        0: '0',
        '1/4': '25vh',
        '1/2': '50vh',
        '3/4': '75vh',
        '4/5': '80vh',
        '5/6': '83vh',
        '9/10': '90vh',
        '19/20': '95vh',
        full: '100vh',
      },
      container: {
        maxWidth: {
          DEFAULT: '750px',
          sm: '100%',
          lg: '100%',
          xl: '800px',
          '2xl': '900px',
        },
      },
    },
  },
  plugins: [require('tailwindcss-aspect-ratio'), iOSHeight],
}
