export async function fetchEvents(calendarId, from, to) {
  try {
    const res = await fetch(
      `/api/rental/cocoonvans.com_rfqoudno4k6j4ekdhv9u82rv20@group.calendar.google.com/123/456`,
      {
        method: 'GET',
        headers: {
          'content-type': 'application/json',
        },
        body: JSON.stringify({ 
          calendarId,
          from,
          to
         }),
      }
    )
    const { data } = await res.json()
    return data
  } catch (error) {
    console.error(
      `There was a problem retrieving events from Google with the query ${query}`
    )
    console.error(error)
  }
}
