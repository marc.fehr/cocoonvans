const NavLinks = [
  {
    title: 'Ausbau',
    link: '/ausbau/angebot',
    icon: 'tools',
    alt: 'Custom Vans',
    children: [
      {
        title: 'Angebot',
        link: '/ausbau/angebot',
        alt: 'Hauptseite der Van-Ausbau Seite',
        icon: 'van'
      },
      {
        title: 'Prozess',
        link: '/ausbau/prozess',
        alt: 'Wichtige Dokumente für die Vermietung',
        icon: 'list'
      },
      {
        title: 'Projekte',
        link: '/ausbau/projekte',
        alt: 'Einige unserer schönsten Projekte',
        icon: 'photo'
      },
    ]
  },
  {
    title: 'Shop',
    link: 'https://shop.cocoonvans.com',
    icon: 'shop',
    alt: 'Cocoonvans Online Shop besuchen',
    children: []
  },
  {
    title: 'Vermietung',
    link: '/vermietung',
    icon: 'rental',
    alt: 'Van-Vermietung bei Cocoonvans',
    children: [
      {
        title: 'Kalender',
        link: '/vermietung',
        alt: 'Mietkalender öffnen',
        icon: 'calendar'
      },
      {
        title: 'Unsere Vans',
        link: '/vermietung/vans',
        alt: 'Hauptseite der Van-Vermietung',
        icon: 'van'
      },
      {
        title: 'Downloads',
        link: '/vermietung/downloads',
        alt: 'Wichtige Dokumente für die Vermietung',
        icon: 'download'
      },
      {
        title: 'Mietkodex',
        link: '/vermietung/kodex',
        alt: 'Unsere Spielregeln',
        icon: 'rules'
      },
    ]
  },
  {
    title: 'Über uns',
    link: '/about',
    alt: 'Wer steckt hinter Cocoonvans?',
    icon: 'info',
    children: []
  },
  {
    title: 'Kontakt',
    link: '/contact',
    icon: 'chat',
    alt: 'Sende uns eine Nachricht',
    children: []
  }
]

export default NavLinks