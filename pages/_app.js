import Head from 'next/head'

import { NotificationContextProvider } from '@store/notification-context'
import { RentalContextProvider } from '../store/rental-context'

import Layout from '@components/ui/layout'

import '@styles/tailwind.css'
import '@styles/gallery.css'

function CocoonvansApp({ Component, pageProps }) {
  return (
    <NotificationContextProvider>
      <RentalContextProvider>
        <Head>
          <title>Cocoonvans</title>
          <link rel='icon' href='/favicon.ico' />
        </Head>
        <Layout>
          <Component {...pageProps} />
        </Layout>
      </RentalContextProvider>
    </NotificationContextProvider>
  )
}

export default CocoonvansApp
