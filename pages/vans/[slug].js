import { useContext, useEffect, useState } from 'react'

import RentalContext from '@store/rental-context'
import NotificationContext from '@store/notification-context'

import Moment from 'moment'
import { extendMoment } from 'moment-range'

import { fetchContent } from '@utils/contentful'

import Richtext from '@components/ui/richtext'
import TextContainer from '@components/ui/text-container'
import DatePicker from '@components/calendar/date-picker'
import RentalForm from '@components/forms/rental-form'

function VanPage(props) {
  const { van } = props

  const moment = extendMoment(Moment)

  const rentalCtx = useContext(RentalContext)
  const notificationCtx = useContext(NotificationContext)

  const datesAreSet = rentalCtx.dateFrom && rentalCtx.dateTo

  const [events, setEvents] = useState([])

  const setCalendarEvents = (e) => {
    const allEvents = e.events
    const blockedDaysArray = []
    for (let i = 0; i < allEvents.length; i++) {
      const start =
        moment(allEvents[i].start.date) || moment(allEvents[i].start.dateTime)
      const end =
        moment(allEvents[i].end.date) || moment(allEvents[i].end.dateTime)
      const r1 = moment.range(start, end)
      const dateArray = Array.from(r1.by('days')).map((m) => m)
      for (let ii = 0; ii < dateArray.length - 1; ii++) {
        if (dateArray[ii].isAfter(moment().subtract(1, 'days'))) {
          blockedDaysArray.push(dateArray[ii])
        }
      }
    }
    setEvents(blockedDaysArray)
  }

  const [isAvailable, setIsAvailable] = useState(true)
  const [isFetching, setIsFetching] = useState(false)

  useEffect(() => {
    // 0 Set data for booking from ctx
    const dateFrom = rentalCtx.dateFrom?.format('YYYY-MM-DD')
    const dateTo = rentalCtx.dateTo?.format('YYYY-MM-DD')
    const rangeBooking = moment.range(dateFrom, dateTo)

    // 1 Set the fetching state
    setIsFetching(true)
    setIsAvailable(true)

    notificationCtx.showNotification({
      title: 'Lade Daten...',
      message: 'Bitte warten',
      status: 'pending',
    })

    // 2 Check all vans for availability with their cals
    fetch(`/api/rental/${van.calendarId}/${dateFrom}/${dateTo}`)
      .then((res) => res.json())
      .then((data) => {
        for (let i = 0; i < data.events.length; i++) {
          const bookedFrom = data.events[i].start.date
          const bookedTo = data.events[i].end.date
          const rangeBooked = moment.range(bookedFrom, bookedTo)

          const overlap = rangeBooking.overlaps(rangeBooked)

          // 3 Set availability based on moment range check
          if (overlap) {
            console.log(rangeBooked)
            setIsAvailable(false)
          }
        }
        setIsFetching(false)
        notificationCtx.hideNotification()
      })
  }, [rentalCtx, van])

  useEffect(() => {
    fetch(
      `/api/rental/${van.calendarId}/rentalCtx.dateFrom?.format('YYYY-MM-DD')/rentalCtx.dateTo?.format('YYYY-MM-DD')`
    )
      .then((res) => res.json())
      .then((data) => {
        setCalendarEvents(data)
      })
  }, [rentalCtx, van])

  return (
    <TextContainer>
      <div className={'rounded-lg overflow-hidden'}>
        <div className={'bg-yellow-400 py-2 px-3'}>
          <h1 className={'m-0'}>{van.title}</h1>
          <h2 className={'m-0 mt-1'}>{van.description}</h2>
        </div>
        <div className={'py-2 px-3 bg-gray-100 rounded-b-lg'}>
          <Richtext content={van.text} />
        </div>
      </div>
      <div
        className={'bg-yellow-400 shadow-lg my-8 rounded-lg dark:text-gray-800'}
      >
        <div className={'px-5 py-2'}>
          <h1 className={'mt-2'}>1. Mietdauer festlegen</h1>
          <p>
            Wähle ein Zeitfenster für die Van-Miete aus. Unsere Mindestmietdauer
            beträgt vier Tage.
          </p>
        </div>
        <DatePicker calendarEvents={events} />
      </div>
      {datesAreSet && isAvailable && <RentalForm van={van.title} />}
    </TextContainer>
  )
}

export default VanPage

export async function getStaticPaths() {
  const van = await fetchContent(`
  {
    rentalVanCollection {
      items {
        slug
      }
    }
  }
  `)

  const slugs = van.rentalVanCollection.items.map((item) => item.slug)
  const paramsWithSlugs = slugs.map((slug) => ({
    params: {
      slug: slug,
    },
  }))

  return {
    paths: paramsWithSlugs,
    fallback: false,
  }
}

export async function getStaticProps(context) {
  const slug = context.params.slug

  const van = await fetchContent(`
  {
    rentalVanCollection(limit: 1, where: {slug: "${slug}"}) {
      items {
      title
        verfgbar
        slug
        text {
          json
          links {
            assets {
              block {
                sys {
                  id
                }
                title
                description
                contentType
                fileName
                size
                url
                width
                height
              }
            }
          }
        }
        description
        slug
        calendarId
        galleryCollection {
          items {
            title
            description
            contentType
            fileName
            size
            url
            width
            height
          }
        }
      }
    }  
  }
  `)

  if (!van) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      van: van.rentalVanCollection.items[0],
    },
    revalidate: 600,
  }
}
