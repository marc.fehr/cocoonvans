import { fetchContent } from '@utils/contentful'

import RichText from '@components/ui/richtext'
import TextContainer from '@components/ui/text-container'

export default function Home(props) {
  const { page } = props // From Contentful

  return (
    <TextContainer>
      <RichText content={page.text} />
    </TextContainer>
  )
}

export async function getStaticProps() {
  const response = await fetchContent(`
  {
    page(id: "trxyi8WhGRH2pCi30pfHn") {
      title
          slug
          filesCollection {
            items {
              title
              description
              contentType
              fileName
              size
              url
              width
              height
            }
          }
          metaDescription
          text {
            json
              links {
                assets {
                  block {
                    sys {
                      id
                    }
                    title
                    description
                    contentType
                    fileName
                    size
                    url
                    width
                    height
                  }
                }
              }
          }
    }
  }
  `)

  return {
    props: {
      page: response.page,
    },
    revalidate: 600,
  }
}
