import { useContext } from 'react'

import { fetchContent } from '@utils/contentful'
import RentalContext from '@store/rental-context'

import VanCard from '@components/ui/card-van'
import Richtext from '@components/ui/richtext'
import TextContainer from '@components/ui/text-container'
import DatePicker from '@components/calendar/date-picker'

function VermietungPage(props) {
  const { page, vans } = props

  const rentalCtx = useContext(RentalContext)

  return (
    <TextContainer>
      <Richtext content={page.text} />
      <div
        className={'bg-yellow-200 dark:bg-gray-700 shadow-lg my-8 rounded-sm '}
      >
        <div className={'px-5 py-2'}>
          <h1 className={'mt-2'}>1. Mietdauer wählen</h1>
          <p>
            Wähle ein Zeitfenster für die Van-Miete aus. Unsere Mindestmietdauer
            beträgt vier Tage.
          </p>
        </div>
        <DatePicker calendarEvents={[]} />
      </div>
      <div
        className={'bg-yellow-200 dark:bg-gray-700 shadow-lg my-8 rounded-sm'}
      >
        <div className={'px-5 py-2'}>
          <h1 className={'mt-2'}>2. Verfügbare Vans</h1>
          <p>Die folgenden Vans stehen für dein Abenteuer bei uns bereit:</p>
        </div>
        {rentalCtx.dateFrom && rentalCtx.dateTo ? (
          <div className={`px-5 py-2 bg-gray-50 dark:bg-gray-600`}>
            {vans.map((van, i) => (
              <VanCard data={van} key={`van-card-${i}`} />
            ))}
          </div>
        ) : (
          <div className={`px-5 py-2 bg-gray-50 dark:bg-gray-600`}>
            <p>Bitte zuerst Mietdauer in Schritt eins festlegen...</p>
          </div>
        )}
      </div>
    </TextContainer>
  )
}

export default VermietungPage

export async function getStaticProps() {
  const response = await fetchContent(`
  {
    rentalVanCollection {
      items {
        title
        slug
        calendarId
        galleryCollection(limit: 3) {
          items {
            title
            fileName
            url
            width
            height
          }
        }
      }
    }
    page(id: "22Yi0kXgXMaIJf7tRukuzd") {
      title
      slug
      metaDescription
      text {
        json
        links {
          assets {
            block {
              sys {
                id
              }
              title
              description
              contentType
              fileName
              size
              url
              width
              height
            }
          }
        }
      }
    }
  }
  
  `)

  if (!response) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      page: response.page,
      vans: response.rentalVanCollection.items,
    },
    revalidate: 600,
  }
}
