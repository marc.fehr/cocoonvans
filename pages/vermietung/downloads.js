import { fetchContent } from '@utils/contentful'

import Downloads from '@components/ui/downloads'
import Richtext from '@components/ui/richtext'
import TextContainer from '@components/ui/text-container'

function VermietungSubPage(props) {
  const { data } = props

  return (
    <div>
      <TextContainer>
        <h1>{data.title}</h1>
        <p>{data.description}</p>
        <Richtext content={data.text} />
        {data.filesCollection.items.length > 0 && (
          <Downloads content={data.filesCollection.items} />
        )}
      </TextContainer>
    </div>
  )
}

export default VermietungSubPage

export async function getStaticProps() {
  const response = await fetchContent(`
  {
    page(id: "GZfmD15zFXtNOUzZAn7tE") {
      title
          slug
          filesCollection {
            items {
              title
              description
              contentType
              fileName
              size
              url
              width
              height
            }
          }
          metaDescription
          text {
            json
              links {
                assets {
                  block {
                    sys {
                      id
                    }
                    title
                    description
                    contentType
                    fileName
                    size
                    url
                    width
                    height
                  }
                }
              }
          }
    }
  }
  `)

  if (!response) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      data: response.page,
    },
    revalidate: 600,
  }
}
