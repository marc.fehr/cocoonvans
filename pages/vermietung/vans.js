import { fetchContent } from '@utils/contentful'

import StyledLink from '@components/ui/link'
import Richtext from '@components/ui/richtext'
import TextContainer from '@components/ui/text-container'

function VansPage(props) {
  const { page, vans } = props

  return (
    <TextContainer>
      <Richtext content={page.text} />
      {vans.map((van, i) => (
        <div className={'border-b-2 pb-5 last:border-0'}>
          <h1>{van.title}</h1>
          <StyledLink href={`/vans/${van.slug}`} text={'Jetzt buchen'} />
        </div>
      ))}
    </TextContainer>
  )
}

export default VansPage

export async function getStaticProps() {
  const response = await fetchContent(`
  {
    rentalVanCollection {
      items {
        title
        slug
        calendarId
        galleryCollection(limit: 3) {
          items {
            title
            fileName
            url
            width
            height
          }
        }
      }
    }
    page(id: "3Q1ItQMonXZ4w8pFo6aqFJ") {
      title
      slug
      metaDescription
      text {
        json
        links {
          assets {
            block {
              sys {
                id
              }
              title
              description
              contentType
              fileName
              size
              url
              width
              height
            }
          }
        }
      }
    }
  }
  
  `)

  if (!response) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      page: response.page,
      vans: response.rentalVanCollection.items,
    },
    revalidate: 600,
  }
}
