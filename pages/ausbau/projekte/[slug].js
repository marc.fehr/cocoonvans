import Image from 'next/image'

import { fetchContent } from '@utils/contentful'

import Richtext from '@components/ui/richtext'
import TextContainer from '@components/ui/text-container'
import ProjectGallery from '@components/projects/project-gallery'

function ProjectPage(props) {
  const { data } = props

  return (
    <>
      <TextContainer>
        <div className={'rounded-lg overflow-hidden'}>
          <div className={'mt-0 bg-yellow-400 py-2 px-3'}>
            <h1 className={'m-0'}>{data.title}</h1>
            <h2 className={'m-0 mt-1'}>{data.subtitle}</h2>
          </div>
          <Image
            src={data.heroImage.url}
            width={data.heroImage.width}
            height={data.heroImage.height}
            objectFit={'cover'}
            // layout={'fill'}
            placeholder={'blur'}
            blurDataURL={`${data.heroImage.url}?w=8`}
          />
          <div className={'-mt-2 py-2 px-3 bg-gray-200 rounded-b-lg'}>
            <Richtext content={data.text} />
          </div>
        </div>
        <div className={'max-w-4xl'}>
          <ProjectGallery images={data.galleryCollection.items} />
        </div>
      </TextContainer>
    </>
  )
}

export default ProjectPage

export async function getStaticPaths() {
  const data = await fetchContent(`
  {
    rentalVanCollection {
      items {
        slug
      }
    }
  }
  `)

  const slugs = data.rentalVanCollection.items.map((item) => item.slug)
  const paramsWithSlugs = slugs.map((slug) => ({
    params: {
      slug: slug,
    },
  }))

  return {
    paths: paramsWithSlugs,
    fallback: 'blocking',
  }
}

export async function getStaticProps(context) {
  const slug = context.params.slug

  const data = await fetchContent(`
  {
    postCollection(limit: 1, where: {slug: "${slug}"}) {
      items {
        title
        slug
        publishDate
        heroImage {
          title
          description
          fileName
          url
          width
          height
        }
        galleryCollection(limit: 25) {
          items {
            url
            width
            height
            description
            title
          }
        }
        body
        text {
          json
        }
        kunde
        fahrzeug
        dauer
        fertigstellung
        tagsCollection(limit: 25) {
          items {
            title
            slug
          }
        }
      }
    }
  }
  `)

  if (!data) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      data: data.postCollection.items[0],
    },
    revalidate: 600,
  }
}
