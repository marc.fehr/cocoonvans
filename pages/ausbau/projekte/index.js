import { fetchContent } from '@utils/contentful'

import Downloads from '@components/ui/downloads'
import Richtext from '@components/ui/richtext'
import TextContainer from '@components/ui/text-container'
import CardProject from '@components/projects/card-project'

function VermietungSubPage(props) {
  const { data, projects } = props

  const allProjects = projects.items

  return (
    <TextContainer>
      <h1>{data.title}</h1>
      <p>{data.description}</p>
      <Richtext content={data.text} />
      {data.filesCollection.items.length > 0 && (
        <Downloads content={data.filesCollection.items} />
      )}
      {allProjects.map((el) => {
        return <CardProject data={el} />
      })}
    </TextContainer>
  )
}

export default VermietungSubPage

export async function getStaticProps() {
  const response = await fetchContent(`
  {
    page(id: "7tgKArZFTZGIVXAvV8qb7T") {
      title
          slug
          filesCollection {
            items {
              title
              description
              contentType
              fileName
              size
              url
              width
              height
            }
          }
          metaDescription
          text {
            json
              links {
                assets {
                  block {
                    sys {
                      id
                    }
                    title
                    description
                    contentType
                    fileName
                    size
                    url
                    width
                    height
                  }
                }
              }
          }
    }
  }
  `)

  const projectResponse = await fetchContent(`
  {
    postCollection(limit: 50) {
      items {
        title
        slug
        publishDate
        heroImage {
          title
          description
          fileName
          url
          width
          height
        }
        galleryCollection(limit: 25) {
          items {
            url
            width
            height
            description
            title
          }
        }
        body
        text {
          json
        }
        kunde
        fahrzeug
        dauer
        fertigstellung
        tagsCollection(limit: 25) {
          items {
            title
            slug
          }
        }
      }
    }
  }
  `)

  if (!projectResponse) {
    return {
      notFound: true,
    }
  }

  if (!response) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      data: response.page,
      projects: projectResponse.postCollection,
    },
    revalidate: 600,
  }
}
