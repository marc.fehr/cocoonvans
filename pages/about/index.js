import { fetchContent } from '@utils/contentful'

import Richtext from '@components/ui/richtext'
import TextContainer from '@components/ui/text-container'
import CardStaff from '@components/ui/card-staff'

function AboutPage(props) {
  const { page, staff } = props

  return (
    <div>
      <TextContainer>
        <section className={'mb-8'}>
          <h1>{page.title}</h1>
          <Richtext content={page.text} />
        </section>
        <section className={'mb-8'}>
          <h1>Unser Team</h1>
          <div className={'-mx-0 mt-5 sm:-mx-5 md:-mx-30 lg:-mx-32 xl:-mx-40'}>
            {staff.map((staffMember, i) => (
              <CardStaff data={staffMember} key={`staff-member-card-${i}`} />
            ))}
          </div>
        </section>
      </TextContainer>
    </div>
  )
}

export default AboutPage

export async function getStaticProps() {
  const response = await fetchContent(`
    {
      staffCollection(order: reihenfolge_ASC) {items {
        name
        firstname
        title
        email
        instagram
        image {
          title
          description
          contentType
          fileName
          size
          url
          width
          height
        }
        text {
          json
        }
      }}
      page(id: "6OEW39sVvk9Fd389xH4Gsy") {
        title
        text {
          json
          links {
            assets {
              block {
                sys {
                  id
                }
                title
                description
                contentType
                fileName
                size
                url
                width
                height
              }
            }
          }
        }
      }
    }
  `)

  if (!response) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      page: response.page,
      staff: response.staffCollection.items,
    },
    revalidate: 600,
  }
}
