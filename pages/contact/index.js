import { fetchContent } from '@utils/contentful'

import Richtext from '@components/ui/richtext'
import TextContainer from '@components/ui/text-container'
import ContactForm from '@components/forms/contact-form'

function ContactPage(props) {
  const { page } = props

  return (
    <TextContainer>
      <h1>{page.title}</h1>
      <Richtext content={page.text} />
      <ContactForm />
    </TextContainer>
  )
}

export async function getStaticProps() {
  const response = await fetchContent(`
    {
      page(id: "SHlIHyLBvxasKhOeespMw") {
        title
        text {
          json
          links {
            assets {
              block {
                sys {
                  id
                }
                title
                description
                contentType
                fileName
                size
                url
                width
                height
              }
            }
          }
        }
      }
    }
  `)

  if (!response) {
    return {
      notFound: true,
    }
  }

  return {
    props: {
      page: response.page,
    },
    revalidate: 600,
  }
}

export default ContactPage
