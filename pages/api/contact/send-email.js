export default function handler(req, res) {
  if (req.method === 'POST') {
    const Recipient = require('mailersend').Recipient
    const EmailParams = require('mailersend').EmailParams
    const MailerSend = require('mailersend')

    const mailersend = new MailerSend({
      api_key: process.env.NEXT_MAILERSEND_ACCESS_TOKEN,
    })

    const recipients = [new Recipient('marc.fehr@gmail.com', 'Marc Fehr')]

    const data = {
      ...req.body,
    }

    const emailParams = new EmailParams()
      .setFrom('cocoonvans@pixelpoetry.dev')
      .setFromName('Cocoonvans Mailer')
      .setRecipients(recipients)
      .setSubject('Kontaktformular')
      .setTemplateId('x2p0347qxylzdrn7')
      .setVariables([
        {
          email: 'marc.fehr@gmail.com',
          substitutions: [
            {
              var: 'message',
              value: data.message || 'No message entered',
            },
            {
              var: 'date',
              value: data.date || 'No date set',
            },
            {
              var: 'name',
              value: data.name || 'No name set',
            },
            {
              var: 'email',
              value: data.email || 'No email set',
            },
            {
              var: 'phone',
              value: data.phone || 'No phone set',
            },
            {
              var: 'city',
              value: data.city || 'No place set',
            },
          ],
        },
      ])

    mailersend.send(emailParams).then((result, error) => {
      console.log(result)
      if (error) {
        res.status(500).json({ status: 'error', error: error })
        return
      } else {
        if (result.statusText === 'Accepted' || result.status === '202') {
          // console.log(result)
          res.status(200).json({ status: result })
          return
        }
      }
    })
    // res.status(200).json({ status: 'Email sent' })
    return
  } else {
    res.status(200).json({ status: 'GET request received' })
    return
  }
}
