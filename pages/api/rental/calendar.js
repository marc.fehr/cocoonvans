export default async (req, res) => {
  const url = `https://www.googleapis.com/calendar/v3/calendars/cocoonvans.com_rfqoudno4k6j4ekdhv9u82rv20@group.calendar.google.com/events?key=AIzaSyBjZF8cdH1cTPvfyWEbvRnPj5I6ArnMMHQ`
  let events

  await new Promise((resolve) => {
    fetch(url)
      .then((res) => res.json())
      .then((data) => (events = data))
      .then(() => resolve())
  })

  res.status(200).json({ events, message: 'Events fetched' })
}
