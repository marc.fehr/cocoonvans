import getConfig from 'next/config'
const { serverRuntimeConfig } = getConfig()

export default async (req, res) => {
  const { googleCalendarKey } = serverRuntimeConfig

  const {
    query: { data },
    method,
  } = req

  const calendarId = data[0]
  const booking = {
    from: data[1],
    to: data[2],
  }

  switch (method) {
    case 'GET':
      const url = `https://www.googleapis.com/calendar/v3/calendars/${calendarId}/events?key=${googleCalendarKey}`

      await new Promise((resolve) => {
        fetch(url)
          .then((res) => res.json())
          .then((data) => {
            console.log('Events fetched!')

            if (data.items.length < 1) {
              res.status(400).json({ message: 'No events found' })
              resolve()
            } else {
              res.status(200).json({ events: data.items, message: 'Success' })
              resolve()
            }
          })
      })
      break
    default:
      res.setHeader('Allow', ['GET', 'POST'])
      res.status(405).end(`Method ${method} Not Allowed`)
  }
}
