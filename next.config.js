module.exports = {
  images: {
    domains: ['ctfassets.net', 'images.ctfassets.net'],
  },
  serverRuntimeConfig: {
    googleCalendarKey: process.env.NEXT_GOOGLE_CALENDAR_KEY,
  },
}
