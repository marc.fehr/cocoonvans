import { createContext, useState } from 'react'

const RentalContext = createContext({
  dateFrom: null,
  dateTo: null,
  dateFromHandler: function (from) {},
  dateToHandler: function (to) {},
})

export function RentalContextProvider(props) {
  const [dateFrom, setDateFrom] = useState()
  const [dateTo, setDateTo] = useState()

  function dateFromHandler(from) {
    setDateFrom(from)
  }

  function dateToHandler(to) {
    setDateTo(to)
  }

  function dateResetHandler() {
    setDateFrom(null)
    setDateTo(null)
  }

  const context = {
    dateFrom: dateFrom,
    dateTo: dateTo,
    dateFromHandler: dateFromHandler,
    dateToHandler: dateToHandler,
    dateResetHandler: dateResetHandler,
  }

  return (
    <RentalContext.Provider value={context}>
      {props.children}
    </RentalContext.Provider>
  )
}

export default RentalContext
